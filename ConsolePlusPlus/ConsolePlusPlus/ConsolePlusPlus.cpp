// ConsolePlusPlus.cpp : Defines the entry point for the console application.

// Includes, which basically work as the libraries of C++! (Are headers different from libraries?)
#include "stdafx.h"
#include <string>
#include <sstream>
#include <iostream>
#include <thread>
#include <chrono>
#include <array>

// Namespaces are also a returning concept. They are basically a scope that can be used at any time.
// Namesspaces can either be included directly in a class, or holded claing namespaceName::value/function.
namespace globalValues
{
	int globalValueA = 300;
	int globalValueB = 200;

	int returnRandom()
	{
		return rand();
	}
}

// Another example namespace.
namespace anotherValue {
	int anotherValueOne = 32;
}

// Namespaces return! In this case, std is a C++ namespace, while globalValues is one declared by us.
using namespace std;
using namespace globalValues;

// Oh yeah, this is the first time that I'm writting C++.

// VARIABLES! REFERENCE VARIABLES! http://i.imgur.com/kfYldZL.png

// Signed means they can support negative values. Unsigned cannot. However, using unsigned shifts the maxValue from 32767 to 65535 due to
// byte adresses.

// They're also not needed, and can be used to like any other variable and be identified only as int, bool, long, ...

string welcomeString = "WELCOME!";
int firstValue;
float secondValue;
const double pi = 3.14159; // The const keyword indicates a constant. 

// Identifiers can also be used, and replaces the sequencer of characthers with a specific command. These are pre-processed. 
#define NEWLINE '\n'

// There are three was to declare variables using C++, they all do the same.
int typeOne = 1;
int typeTwo(2);
int typeThree{ 3 };
int randomValue = 32;

// Arrays again! Arrays can also determine it's own size based on the amount of values intialized.
int setOfValues[5] = { 10, 20, 30 }; // Values 3 and 4 are set to 0.
int setOfValuesAgain[] = { 40, 50 }; // These array is going to have a 2 element size.

/*
In C++, it is syntactically correct to exceed the valid range of indices for an array. 
This can create problems, since accessing out-of-range elements do not cause errors on compilation, but can cause errors on runtime.
The reason for this being allowed will be seen in a later chapter when pointers are introduced.
*/

// auto and decltype(anotherVariable). nullptr is the equivalent to a null value.

// A new suprise! In C++ functions have to be defined like they were variables before they can be called!
// One way to do this is to define the functions before main, but for reading sakes, they can be also be defined
// by placing them with semi-colons, like this!
int RequestGeneration();
void GenerateNumbers(int& numberGenerates);

// Functions are declared exactly the same way. Main() is a special function and is run always on start up.
void main()
{
	// In C++, foreach loops are actually a type of for loops. In this case, we use a string, which is a sequence of char.
	// It's syntax is for (declaration : range) 
	for (char stringChr : welcomeString)

	{
		cout << "[" << stringChr << "]";
		this_thread::sleep_for(chrono::milliseconds(500));
	}

	// Prints a string. Note that STD just contains the extra types do to <iostream>
	// The << are data insertions. The COUT and CIN are examples of streams. 
	cout << NEWLINE << "This is a first C++ program!" << endl;
	// The adress of a variable can be obtained by using the ampersand sign before the variable name.
	cout << "Also, heres the memory adress of that PI number! " << &pi << endl;
	system("pause");

	// The mystery behind ++ has also been revealed! Depending on how it's used to equals, they can give out different values.
	int newValue = randomValue++; // In this case newValue is going to be 32, and randomValue is going to become 33 right afterwards.
	int newValue2 = ++randomValue; // In this case newValue2 is going to be 34, as randomValue was increemented beforehand. (Twice)

	cout << NEWLINE << "Here's the difference between the ++ position! " << newValue << " and " << newValue2 << endl;
	system("pause");

	// There's also a comma operator , which executes multiple tasks where only one expression is expected.
	// A new concept is are also inner scopes. These are code sections which are executed, similiarly to a function.
	// However, are placable inside functions. These also follow local variables, as in the following example
	// The newValue variable is accounted as a new variable despite being used previously.
	{
		// Here's an example on namespaces, where we use globalValueA directly because we're already using the namespace.
		// and we're we call it manualy (and where we would have not to declare it). Namespaces can also be called specifically
		// for specific code scopes.
		using namespace anotherValue;
		int newValue = globalValueA + globalValues::returnRandom() + anotherValueOne; 
		cout << NEWLINE << "The number in the inside scope is " << newValue << endl;
		system("pause");
	}

	// Arrays are mutually conected to the concept of pointers. In fact [] is a offest operator, which in the array case,
	// offsets a pointer value by the value in the bracket.
	{
		cout << NEWLINE << "----------------------------------------" << endl;
		int pointerNumbers[5] = { 15, 25, 35, 45, 55 };
		int * pointer;

		// Here we are making the pointer we defined equal to the pointer of the array, and then offseting it to get it's value.
		pointer = pointerNumbers;
		*(pointer + 4) = 65;
		*(pointer + 5) = rand();
		pointer = pointer + 5;

		for (int numberArray : pointerNumbers)
		{
			cout << "By the way, here's a value from an array: " << numberArray << " - Adress: " << &numberArray << endl;
		}
		cout << "Also a random value straight from a pointer! " << *pointer << " - Adress: "<< pointer << endl;
		system("pause");
	}

	// Prints elements from an array to a for loop (or a foreach, whatever you call it.)
	cout << NEWLINE << "----------------------------------------" << endl;

	cout << NEWLINE << "Have I mentioned I like arrays?" << endl;

	for (int currentInt : setOfValues)
	{
		cout << "[" << currentInt << "]";
		this_thread::sleep_for(chrono::milliseconds(500));
	}

	cout << NEWLINE;
	system("pause");
	cout << NEWLINE << "----------------------------------------" << endl;

	// As stated before, CIN can be used to give the user some input over the program. Now with a do loop!
	int numberGenerates = RequestGeneration();
	GenerateNumbers(numberGenerates);

	cout << NEWLINE << "----------------------------------------" << endl;

	// Regrading strings, there's also two extra things that can be used. getline(cin, variableOutput) for example, allows a string to be
	// inputed with a cin, without it breaking do to invalid characthers. stringstream(variableOutput) converts, say, a numberic variable
	// into a a stremed value.
	string nameOut;
	cout << NEWLINE << "Cool! Now what's your name to sign this program? ";
	cin.ignore(); // cin.Ignore() ignores all previous whitespaces, allowing for re-input.
	getline (cin, nameOut);
	cout << NEWLINE << "Thanks for learning with me, " << nameOut << "!" << endl;
	cout << NEWLINE << "Seems like the program is done! Keep learning!" << endl;
	
	system("pause");
}

// Function used to quest a generation.
int RequestGeneration ()
{
	int numberGenerates = 0;
	do
	{
		if (numberGenerates > 20)
		{
			cout << "Whowa! Calm down there buddy. Maybe that is a little too much?" << endl;
		}
		else if (numberGenerates < -20)
		{
			cout << "I do support negative numbers, maybe not so down tough." << endl;
		}

		cout << NEWLINE << "Now, how many numbers would you like to be generated? ";
		cin >> numberGenerates;
		cout << endl;
	} while (numberGenerates == 0 || numberGenerates > 20 || numberGenerates < -20);

	// Returns the value to the main function.
	return numberGenerates;
}

// Another new concept in (which is appliable in C# with ref) are references, in this case, the values given to the functions
// are references, and not duplicates, which means changes made to the variable in the function will be reflected in the given variables.
// This can be cost efficient, as in this case, there's no need to copy the value. To avoid accidental changes to the function,
// const can also be added to the parameter, meaning the value cannot be eddited inside the function.
void GenerateNumbers (int& numberGenerates)
{
	if (numberGenerates < 0)
	{
		cout << "Hmm... seems like you introduced a negative input, so I just converted it for you, kay?" << endl;
		numberGenerates *= -1;
	}

	// Generates two values. Loops return in the exact same way... again.
	for (int i = 0; i < numberGenerates; i++)
	{
		cout << NEWLINE << "----------------------------------------" << endl;

		cout << NEWLINE << "This is generation No. " << i + 1 << endl;

		firstValue = rand();
		secondValue = rand();

		// These print functions seem to use << instead of + as other strings. Why? Let's figure out!
		cout << "First Value is " << firstValue << " and the Second Value " << secondValue << endl;

		float result = firstValue + secondValue;

		cout << "The sum is " << result << endl;

		// Does a teneray operator depending on the result of the condition
		string conditionalResult = result > 20000
			? "It's over 20000!" : "It's under 20000!";

		// Conditions work exactly the same way as they used too!
		if (result > 50000)
		{
			cout << "Whew! It's pretty big actually!" << endl;
		}

		cout << "Also, the result was... " << conditionalResult << endl;

		// sizeof returns the bit size of a variable. Don't know if it will be useful, but better keep track of it!
		cout << "The string was also this byte long - " << sizeof(conditionalResult) << endl;

		system("pause");
	}
}

// Another new concept for functions here is inline. This suggests the compiler that the code could possibily be inserted into the place
// the function is called from instead of making a normal function call. For example, a simple function like addition could be made inline
// which would basically place the function is main() at the place it was called (instead of doing the whole normal function call).
// However, there is no need to do this at all functions which are short or simple, as the compiler will do that automatically.